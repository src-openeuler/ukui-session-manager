Name:           ukui-session-manager
Version:        4.0.0.0
Release:        5
Summary:        Session manager of the UKUI desktop environment
License:        GPL-2.0-or-later and GPL-3.0-or-later and LGPL-2.0-or-later 
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz

Patch01:	0001-change-CMAKE_INSTALL_SYSCONFDIR-to-etc.patch
Patch02:	0001-fix-install-error-of-ukui-session-manager-4.0.0.0.patch

BuildRequires: cmake
BuildRequires: glib2-devel 
BuildRequires: gsettings-qt-devel
BuildRequires: kf5-kconfig-devel
BuildRequires: kf5-kcoreaddons-devel
BuildRequires: kf5-kidletime-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: libSM-devel
BuildRequires: ukui-interface
BuildRequires: libX11-devel
BuildRequires: libXtst-devel
BuildRequires: pkg-config
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtmultimedia-devel
BuildRequires: qt5-qttools-devel
BuildRequires: qt5-qttools-devel
BuildRequires: xdg-user-dirs
BuildRequires: kf5-kwayland-devel
BuildRequires: libkscreen-qt5-devel
BuildRequires: libkysdk-sysinfo-devel

Requires: ukui-themes
Requires: glib2
Requires: qt5-qtmultimedia

Recommends: peony
Recommends: ukui-kwin
Recommends: ukui-panel
Recommends: ukui-polkit
Recommends: ukui-screensaver
Recommends: ukui-settings-daemon

Provides: x-session-manager

%description
 This package contains a session that can be started from a display
 manager such as lightdm. It will load all necessary applications for
 a full-featured user session.
 This package contain the session manager component.


%package -n ukui-session-wayland
Summary:     ession of the UKUI desktop environment in wayland
License:     LGPL-3.0-or-later and MIT and BSD-3-Clause
Requires:    ukui-session-manager
Requires:    ukui-kwin-wayland

%description -n ukui-session-wayland
Session of the UKUI desktop environment in wayland

%prep
%autosetup -n %{name}-%{version} -p1

%build
%cmake
%{cmake_build}

%install
%cmake_install

#install -d   %{buildroot}/usr/share/man/man1/ %{buildroot}/etc/polkit-1/localauthority/50-local.d/
#install -m644 data/com.ubuntu.enable-hibernate.pkla %{buildroot}/etc/polkit-1/localauthority/50-local.d/com.ubuntu.enable-hibernate.pkla
#gzip -c man/ukui-session.1 >  %{buildroot}/usr/share/man/man1/ukui-session.1.gz
#gzip -c man/ukui-session-tools.1 > %{buildroot}/usr/share/man/man1/ukui-session-tools.1.gz

%post
set -e

file=/home/$SUDO_USER/.profile
echo "$file"
if [ -f "$file" ]; then
        sed -i '/export GDK_SCALE=/d' $file
        sed -i '/export QT_SCALE_FACTOR=/d' $file
        sed -i '/export QT_AUTO_SCREEN_SET_FACTOR=/d' $file
fi


%files
%doc debian/changelog debian/copyright
%{_sysconfdir}/X11/Xsession.d/*
%{_sysconfdir}/polkit-1/localauthority/50-local.d/*
%{_sysconfdir}/ukui/ukui-session/*
%{_datadir}/xsessions/
%{_bindir}/ukui-session
%{_bindir}/ukui-session-tools
%{_bindir}/ukuismserver
%{_datadir}/glib-2.0/schemas/
%{_datadir}/ukui/

%files -n ukui-session-wayland
%{_datadir}/wayland-sessions/
%{_bindir}/ukui-session-wayland
%{_datadir}/applications


%changelog
* Tue Nov 26 2024 Funda Wang <fundawang@yeah.net> - 4.0.0.0-5
- adopt to new cmake macro

* Wed Aug 28 2024 douyan <douyan@kylinos.cn> - 4.0.0.0-4
- Type:update
- ID:NA
- SUG:NA
- DESC: fix %post warning issue

* Wed Jun 05 2024 peijiankang <peijiankang@kylinos.cn> - 4.0.0.0-3
- mv kwin-wayland to ukui-kwin-wayland

* Mon May 27 2024 peijiankang <peijiankang@kylinos.cn> - 4.0.0.0-2
- add 0001-fix-install-error-of-ukui-session-manager-4.0.0.0.patch

* Mon May 27 2024 peijiankang <peijiankang@kylinos.cn> - 4.0.0.0-2
- add 0001-fix-install-error-of-ukui-session-manager-4.0.0.0.patch

* Tue Apr 02 2024 huayadong <huayadong@kylinos.cn> - 4.0.0.0-1
- update version to 4.0.0.0

* Thu Sep 21 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.0-6
- Type:bugfix
- ID  :NA
- SUG :NA
- DESC:add patch5: remove-ukui-settings-daemon-quit.patch

* Mon Sep 04 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add patch3:add-switchuser-no-limits-ukui-session.patch
       rm disable-Suspend-and-Sleep-of-ukui-session-manager.patch

* Mon Jul 10 2023 huayadong <huayadong@kylinos.cn> - 3.1.0-4
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:add patch2:ukui-session-manager-3.1.0-kylin-fix-ukui-kwin-x11-coredump.patch

* Wed Jun 14 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.0-3
- Type:bugfix
- ID:NA
- SUG:NA
- DESC: disable Suspend and Sleep of ukui-session-manager

* Tue Feb 07 2023 tanyulong <tanyulong@kylinos.cn> - 3.1.0-2
- Enable debuginfo for fix strip

* Tue Dec 6 2022 peijiankang <peijiankang@kylinos.cn> - 3.1.0-1
- update version to 3.1.0

* Mon Aug 08 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.2-11
- add control dependency and rules

* Thu Aug 04 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.2-10
- update frash style

* Mon May 23 2022 tanyulong<tanyulong@kylinos.cn> - 3.0.2-9
- Improve the project according to the requirements of compliance improvement

* Wed Apr 06 2022 tanyulong <tanyulong@kylinos.cn> - 3.0.2-8
- add yaml file 

* Tue Dec 07 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.2-7
- add patch: 0006-ukui-session-session-application.patch

* Thu Dec 02 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.2-6
- update debian changelog

* Thu Nov 11  2021 tanyulong <tanyulong@kylinos.cn> - 3.0.2-5
- add startlogout signal

* Mon Nov 1  2021 tanyulong <tanyulong@kylinos.cn> - 3.0.2-4
- adjust the sequence to make translation file loaded first

* Thu Oct 28 2021 tanyulong <tanyulong@kylinos.cn> - 3.0.2-3
- reset changelog and format

* Thu Dec 3 2020 lvhan <lvhan@kylinos.cn> - 3.0.2-2
- fix poweroff

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 3.0.2-1
- update to upstream version 3.0.1

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 2.0.2-1
- Init package for openEuler
